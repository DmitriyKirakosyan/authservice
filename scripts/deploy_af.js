const Promise = require('bluebird');
const fs = require('fs-extra');
const replace = require('replace');

let afPath = 'out/azure-function-app';

fs.copy('./AzureFunctions', './out')
    .then(() => {
        return fs.remove('./out/dts')
    })
    .then(() => {
        return fs.copy('package.json', './out/package.json')
    })
    .then(() => {
        return replace({
            paths: ['./out'],
            exclude: 'js',
            regex: '../../out/js/functions/',
            replacement: '../js/functions/',
            recursive: true,
            silent: true
        })
    })
    .catch(err => {
        console.log('err copying files : ', err)
    });