const readline = require('readline');
const Promise = require('bluebird');
const fs = require('fs-extra');
const os = require('os');

let i = readline.createInterface(process.stdin, process.stdout)

let question = Promise.promisify((question, callback) => {
    i.question(question, callback.bind(null, null))
})

let docdbConnectionStringPromise = question('Documentdb Connection String (skip for using local emulator) : ')
let storageConnStringPromise = docdbConnectionStringPromise.then(() => {
    return question('Azure Storage Connection String (local) : ')
})
let allowUnauthConnectionPromise = storageConnStringPromise.then(() => {
    return question('Allow unauthorized tls connection? (y/n) : ')
})

Promise.join(docdbConnectionStringPromise, storageConnStringPromise, allowUnauthConnectionPromise, (docdbConnectionString, storageConnString, allowUnauth) => {

    i.close();

    docdbConnectionString = docdbConnectionString || "AccountEndpoint=https://localhost:8081;AccountKey=C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==;"
    storageConnString = storageConnString || "UseDevelopmentStorage=true";

    fs.writeFile('.env',
        'DOCUMENTDB_CONNECTION_STRING=' + docdbConnectionString + os.EOL +
        'AZURE_STORAGE_CONNECTION_STRING=' + storageConnString + os.EOL +
        'NODE_TLS_REJECT_UNAUTHORIZED=' + (allowUnauth.toLowerCase() == 'y' || allowUnauth.toLowerCase() == 'yes' ? '0' : '1')
    );

})