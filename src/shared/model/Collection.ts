import { Collection as DocdbCollection, CollectionPartitionKey, PartitionKind } from 'documentdb';
import config from '../../DatabaseConfig';

/**
 * Private collection.
 * Each user can get access to their documents only.
 */
export class PrivateCollection implements DocdbCollection {
    id: string;
    partitionKey: CollectionPartitionKey;

    constructor(id: string) {
        this.id = id;
        this.partitionKey = {
            paths: ['/' + config.privateCollectionPartitionKey],
            kind: 'Hash'
        };
    }
}

/**
 * Shared collection.
 * Every user has access to all documents within such collection.
 */
export class SharedCollection implements DocdbCollection {
    constructor(public id: string) {
    }
}