import { DocumentClient, Collection, Permission, AbstractMeta, CollectionMeta, PermissionMeta, DatabaseMeta, QueryError } from 'documentdb';
import { parseConnectionString } from './documentDBUtils';
import config from '../../DatabaseConfig';

export class DocdbDao {

    private client: DocumentClient;

    constructor(connectionString: string) {
        if (!connectionString) {
            throw new Error("Missing connection string");
        }

        let account = parseConnectionString(connectionString);
        if (!account) {
            throw new Error("Connection string is not valid : " + connectionString);
        }

        this.client = new DocumentClient(account.host, { masterKey: account.key });
    }

    /**
     * Create the database
     * @param dbId the database id
     */
    public createDatabase(dbId: string): Promise<DatabaseMeta> {
        return new Promise((resolve, reject) => {
            this.client.createDatabase({ id: dbId }, (err: QueryError, createdDatabase: DatabaseMeta) => {
                if (err) reject(err);
                else resolve(createdDatabase);
            });
        });
    }

    /**
     * Delete the database
     * @param dbId the database id
     */
    public deleteDatabase(dbId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.client.deleteDatabase(this.databaseLink(dbId), (err: QueryError) => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    /**
     * Create collection
     * @param dbId 
     * @param collection 
     */
    public createCollection(dbId: string, collection: Collection): Promise<CollectionMeta> {
        return new Promise((resolve, reject) => {
            let dbLink = this.databaseLink(dbId);
            this.client.createCollection(dbLink, collection, (err, createdCollection) => {
                if (err) reject(err);
                else resolve(createdCollection);
            });
        });
    }

    /**
     * Get collection
     * @param dbId 
     * @param collId 
     */
    public getCollection(dbId: string, collId: string): Promise<CollectionMeta> {
        return new Promise((resolve, reject) => {
            let collLink = this.collectionLink(dbId, collId);
            this.client.readCollection(collLink, (err, collection) => {
                if (err) reject(err);
                else resolve(collection);
            });
        });
    }

    /**
     * Get collections
     * @param dbId 
     * @param collId 
     */
    public getCollections(dbId: string): Promise<Array<CollectionMeta>> {
        return new Promise((resolve, reject) => {
            let collLink = this.databaseLink(dbId);
            this.client.readCollections(collLink).toArray((err, collections) => {
                if (err) reject(err);
                else resolve(collections);
            });
        });
    }

    /**
     * Delete collection
     * @param dbId 
     * @param collId 
     */
    public deleteCollection(dbId: string, collId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let collLink = this.collectionLink(dbId, collId);
            this.client.deleteCollection(collLink, (err) => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    /**
     * Create user
     * @param dbId 
     * @param userId 
     */
    public createUser(dbId: string, userId: string): Promise<AbstractMeta> {
        return new Promise((resolve, reject) => {
            let dbLink: string = this.databaseLink(dbId);
            this.client.createUser(dbLink, { id: userId }, (err, user) => {
                if (err) reject(err);
                else resolve(user);
            });
        });
    }

    /**
     * Get user
     * @param dbId 
     * @param userId 
     */
    public getUser(dbId: string, userId: string): Promise<AbstractMeta> {
        return new Promise((resolve, reject) => {
            let userLink: string = this.userLink(dbId, userId);
            this.client.readUser(userLink, (err, user) => {
                if (err) reject(err);
                else resolve(user);
            });
        });
    }

    /**
     * Get user permission object for given collection
     * @param userId 
     * @param dbId 
     * @param collId 
     */
    public getUserPermission(userId: string, dbId: string, collId: string): Promise<PermissionMeta> {
        return new Promise((resolve, reject) => {
            let permissionId = this.createPermissionId(userId, collId);
            let permissionLink = this.permissionLink(dbId, userId, permissionId);

            this.client.readPermission(permissionLink, (err, permission) => {
                if (err) reject(err);
                else resolve(permission);
            });
        });
    }

    /**
     * Get permission token string for given collection
     * Permission will be created if not found
     * @param userId 
     * @param dbId 
     * @param collId 
     */
    public getOrCreateUserPermissionToken(userId: string, dbId: string, collId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.createUserPermission(userId, dbId, collId
                // this.getUserPermission(userId, dbId, collId).then(
                //     permission => { return permission; },
                //     error => { return this.createUserPermission(userId, dbId, collId); }
            ).then(

                permission => {
                    // winston.debug("[DocdbDao] permission object : ", permission);
                    resolve(permission['_token']);
                },
                error => reject(error)
                );
        });
    }

    /**
     * Create user permission for given collection
     * @param userId 
     * @param dbId 
     * @param collId 
     */
    public createUserPermission(userId: string, dbId: string, collId: string): Promise<PermissionMeta> {
        return new Promise((resolve, reject) => {
            this.getCollection(dbId, collId).then(
                collection => {
                    let userLink = this.userLink(dbId, userId);
                    let permission: Permission = {
                        id: this.createPermissionId(userId, collId),
                        permissionMode: 'All',
                        resource: collection._self
                    };

                    let partitionKey = '/' + config.privateCollectionPartitionKey;
                    if (collection.partitionKey && collection.partitionKey.paths.length > 0 &&
                        collection.partitionKey.paths[0] === partitionKey) {
                        permission['resourcePartitionKey'] = [userId];
                    }


                    this.client.upsertPermission(userLink, permission, (err, permissionMeta) => {
                        if (err) reject(err);
                        else resolve(permissionMeta);
                    });
                },
                err => reject(err)
            );
        });
    }

    // Private

    private databaseLink(dbId: string): string {
        return 'dbs/' + dbId;
    }

    private collectionLink(dbId: string, collId: string): string {
        return this.databaseLink(dbId) + '/colls/' + collId;
    }

    private userLink(dbId: string, userId: string): string {
        return this.databaseLink(dbId) + '/users/' + userId;
    }

    private permissionLink(dbId: string, userId: string, permissionId: string) {
        return this.userLink(dbId, userId) + '/permissions/' + permissionId;
    }

    private createPermissionId(userId: string, collectionId: string): string {
        return userId + '_' + collectionId + '_permission';
    }

}