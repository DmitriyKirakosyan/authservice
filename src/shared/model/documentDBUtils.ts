
export function parseConnectionString(connString: string): { host: string, key: string } {
    let splitted = connString.split(";");
    let host: string;
    let key: string;

    let accountNameStr = "AccountEndpoint=";
    let accountKeyStr = "AccountKey=";

    splitted.forEach(item => {
        if (item.startsWith(accountNameStr))
            host = item.replace(accountNameStr, "");
        if (item.startsWith(accountKeyStr))
            key = item.replace(accountKeyStr, "");
    });

    return host && key ? { host: host, key: key } : undefined;
}
