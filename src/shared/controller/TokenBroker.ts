import { PrivateCollection, SharedCollection } from '../model/Collection';
import { AbstractDBController } from "./AbstractDBController";
import { CollectionMeta, PermissionMeta, AbstractMeta } from 'documentdb';
import config from '../../DatabaseConfig';


export default class TokenBroker extends AbstractDBController {

    /**
     * Get user access token for the given collection
     * @param userId - user id
     * @param collId - collection id
     */
    getUserAccessToken(userId: string, dbId: string, collId: string): Promise<object> {
        return this.ensureUserExists(userId, dbId).then(() => {
            return this.docdb.getOrCreateUserPermissionToken(userId, dbId, collId).then(
                retirivedToken => {
                    return {
                        token: retirivedToken,
                        ttl: config.permissionTokenTTL
                    };
                }
            );
        });
    }

    // Private methods

    private ensureUserExists(userId: string, dbId: string): Promise<any> {
        return this.docdb.getUser(dbId, userId).then(
            found => { return; },
            error => { return this.docdb.createUser(dbId, userId); }
        );
    }
}