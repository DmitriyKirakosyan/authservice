import { PrivateCollection, SharedCollection } from '../model/Collection';
import { AbstractDBController } from "./AbstractDBController";
import { CollectionMeta } from 'documentdb';


export default class CollectionController extends AbstractDBController {

    /**
     * Create private collection
     * @param coll - Collection
     * @param dbId - Database Id
     */
    public createPrivateCollection(collId: string, dbId: string): Promise<CollectionMeta> {
        let coll = new PrivateCollection(collId);
        return this.docdb.createCollection(dbId, coll);
    }

    /**
     * Create shared collection
     * @param coll - Collection
     * @param dbId - Database Id
     */
    public createSharedCollection(collId: string, dbId: string): Promise<CollectionMeta> {
        let coll = new SharedCollection(collId);
        return this.docdb.createCollection(dbId, coll);
    }

    public getCollections(dbId: string): Promise<Array<CollectionMeta>> {
        return this.docdb.getCollections(dbId);
    }

    /**
     * Delete collection
     * @param collId - Collection Id
     * @param dbId   - Database Id
     */
    public deleteCollection(collId: string, dbId: string): Promise<any> {
        return this.docdb.deleteCollection(dbId, collId);
    }
}