import { DocdbDao } from '../model/DocdbDao';

export abstract class AbstractDBController {

    protected docdb: DocdbDao;

    constructor(connectionString: string) {
        this.docdb = new DocdbDao(connectionString);
    }
}