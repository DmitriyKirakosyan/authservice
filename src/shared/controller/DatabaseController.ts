import { AbstractDBController } from "./AbstractDBController";
import { DatabaseMeta } from 'documentdb';

export default class DatabaseController extends AbstractDBController {

    public createDatabase(dbId: string): Promise<DatabaseMeta> {
        return this.docdb.createDatabase(dbId);
    }

    public deleteDatabase(dbId): Promise<any> {
        return this.docdb.deleteDatabase(dbId);
    }
}