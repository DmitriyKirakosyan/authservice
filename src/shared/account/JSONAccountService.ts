import { DatabaseInfo, AbstractAccountService } from "./AbstractAccountService";
import { TSMap } from "typescript-map";
import * as jsonFile from "jsonfile";
import * as fs from 'fs';

const JSON_FILE_NAME = "json_storage.db";

export default class JSONAccountService extends AbstractAccountService {

    constructor() {
        super();
    }

    protected initSystemDB(): void {
        jsonFile.writeFileSync(JSON_FILE_NAME, {}, { spaces: 2 });
    }

    protected isSystemDBInited(): boolean {
        return fs.existsSync(JSON_FILE_NAME);
    }

    protected addDatabaseImpl(appSecret: string, dbConnectionString: string): Promise<DatabaseInfo> {
        return new Promise((resolve, reject) => {
            if (!appSecret) {
                throw new Error("The application secret must be defined");
            }

            let records: any = jsonFile.readFileSync(JSON_FILE_NAME);
            if (records[appSecret]) {
                throw new Error("Database with such application secret already exists");
            }

            let dbName = super.generateDatabaseName();

            records[appSecret] = new DatabaseInfo(dbName, dbConnectionString);

            jsonFile.writeFileSync(JSON_FILE_NAME, records, { spaces: 2 });

            resolve(records[appSecret]);
        });
    }

    protected getDatabaseImpl(appSecret: string): Promise<DatabaseInfo> {
        return new Promise((resolve, reject) => {
            if (!appSecret) {
                throw new Error("The application secret must be defined");
            }

            let records: any = jsonFile.readFileSync(JSON_FILE_NAME);

            resolve(records[appSecret]);
        });
    }

    protected doesAppSeretExistImpl(appSecret: string): boolean {
        if (!appSecret) {
            throw new Error("The application secret must be defined");
        }

        let records: any = jsonFile.readFileSync(JSON_FILE_NAME);
        if (records[appSecret]) {
            return true;
        }
        else {
            return false;
        }
    }

    protected deleteDatabaseImpl(appSecret: string): Promise<DatabaseInfo> {
        return new Promise((resolve, reject) => {
            if (!appSecret) {
                throw new Error("The application secret must be defined");
            }

            let records: any = jsonFile.readFileSync(JSON_FILE_NAME);

            if (!records[appSecret]) {
                throw new Error("Database does not exist");
            }

            let dbInfo = records[appSecret];
            delete records[appSecret];

            jsonFile.writeFileSync(JSON_FILE_NAME, records, { spaces: 2 });
            resolve(dbInfo);
        });
    }
}