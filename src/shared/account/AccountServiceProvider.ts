import JSONAccountService from "./JSONAccountService";
import TablesAccountService from './TablesAccountService';
import { AbstractAccountService } from "./AbstractAccountService";


export default class AccountServiceProvider {

    public static getDefault(): AbstractAccountService {
        switch (process.env.STORAGE_TYPE) {
            case "tables":
                return new TablesAccountService();
            default:
                return new JSONAccountService();
        }
    }

}