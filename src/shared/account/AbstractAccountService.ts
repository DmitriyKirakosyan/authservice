import * as uuid from 'uuid';

const DB_NANE_PREFIX = "db_";

export class DatabaseInfo {
    constructor(public name: string, public connectionString: string) { }
}

/**
 * Account service.
 */
export abstract class AbstractAccountService {

    /**
     * Check system if db exists
     */
    protected abstract isSystemDBInited(): boolean;

    /**
     * Init system database
     */
    protected abstract initSystemDB(): void;

    /**
     * Implementation of adding the database name to storage.
     * @param appSecret the application secret key
     * @returns Database name
     */
    protected abstract addDatabaseImpl(appSecret: string, dbConnectionString: string): Promise<DatabaseInfo>;

    /**
     * Implementation of removing the database name from storage
     * @param appSecret the application secret key
     */
    protected abstract deleteDatabaseImpl(appSecret: string): Promise<DatabaseInfo>;

    /**
     * Implementation of getting the database name
     * @param appSecret the application seret key
     */
    protected abstract getDatabaseImpl(appSecret: string): Promise<DatabaseInfo>;

    /**
     * Implementation of checking of application secret exists
     * @param appSecret the application secret key
     */
    protected abstract doesAppSeretExistImpl(appSecret: string): boolean;

    /**
     * Database Name Generator
     * @returns Database Name
     */
    protected generateDatabaseName(): string {
        return DB_NANE_PREFIX + uuid();
    }

    /**
     * Generate new database name and add database info to storage
     * @param appSecret the application secret key
     * @returns Database Info
     */
    public addDatabase(appSecret: string, dbConnectionString: string): Promise<DatabaseInfo> {
        if (!this.isSystemDBInited()) {
            this.initSystemDB();
        }

        return this.addDatabaseImpl(appSecret, dbConnectionString);
    }

    /**
     * Remove database info from storage
     * @param appSecret the application secret key
     * @returns Database Info
     */
    public deleteDatabase(appSecret: string): Promise<DatabaseInfo> {
        if (!this.isSystemDBInited()) {
            throw new Error("System database not found.");
        }

        return this.deleteDatabaseImpl(appSecret);
    }

    /**
     * Get database info for application secret
     * @param appSecret the application secret key
     * @returns Database Info
     */
    public getDatabase(appSecret: string): Promise<DatabaseInfo> {
        if (!this.isSystemDBInited()) {
            return new Promise((resolve, reject) => { reject(Error("System database not found.")); });
        }

        return this.getDatabaseImpl(appSecret);
    }

    /**
     * Check if application secret exists
     * @param appSecret the application secret key
     */
    public doesAppSeсretExist(appSecret: string): boolean {
        if (!this.isSystemDBInited()) {
            return false;
        }

        return this.doesAppSeretExistImpl(appSecret);
    }
}