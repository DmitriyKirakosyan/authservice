import { DatabaseInfo, AbstractAccountService } from "./AbstractAccountService";
import * as azureStorage from 'azure-storage';
import * as winston from 'winston';

const PARTITION_KEY = "PartitionKey";
const TABLE_NAME = "DatabaseMap";

class AppEntity {
    constructor(
        public dbName: { _: string },
        public dbConnectionString: { _: string })
    { }
}

export default class TablesAccountService extends AbstractAccountService {

    private isInited: boolean = false;
    private tableService: azureStorage.TableService;

    constructor() {
        super();

        this.initSystemDB();
    }

    protected initSystemDB(): void {
        winston.debug('tables storage : ', process.env.AZURE_STORAGE_CONNECTION_STRING);

        this.tableService = azureStorage.createTableService();
        this.isInited = true;
    }

    protected isSystemDBInited(): boolean {
        return this.isInited;
    }

    protected addDatabaseImpl(appSecret: string, dbConnectionString: string): Promise<DatabaseInfo> {
        let dbName = super.generateDatabaseName();

        return new Promise((resolve, reject) => {
            this.tableService.createTableIfNotExists(TABLE_NAME, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                let descriptor = this.createEntityDescriptor(appSecret, dbName, dbConnectionString);
                this.tableService.insertEntity(TABLE_NAME, descriptor, (err, result) => {
                    if (err) {
                        winston.debug("[TablesAccountService] Error inserting app entity : ", err);
                        reject(err);
                    } else {
                        resolve(new DatabaseInfo(dbName, dbConnectionString));
                    }
                });
            });
        });
    }

    protected getDatabaseImpl(appSecret: string): Promise<DatabaseInfo> {
        return new Promise((resolve, reject) => {
            this.tableService.createTableIfNotExists(TABLE_NAME, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                this.tableService.retrieveEntity<AppEntity>
                    (TABLE_NAME, PARTITION_KEY, appSecret, { autoResolveProperties: true }, (err, result) => {
                        if (err) {
                            // winston.debug("[TablesAccountService] Error retriving app entity : ", err);
                            reject(err);
                        } else {
                            // winston.debug("[TablesAccountService] Result retriving app entity : ", result);
                            let connectionString = result.dbConnectionString ? result.dbConnectionString._ : undefined;
                            resolve(new DatabaseInfo(result.dbName._, connectionString));
                        }
                    });
            });
        });
    }

    protected doesAppSeretExistImpl(appSecret: string): boolean {
        return false;
    }

    protected deleteDatabaseImpl(appSecret: string): Promise<DatabaseInfo> {
        return new Promise((resolve, reject) => {
            this.getDatabaseImpl(appSecret).then(
                (dbInfo) => {
                    let descriptor = this.createEntityDescriptor(appSecret);
                    this.tableService.deleteEntity(TABLE_NAME, descriptor, (err, result) => {
                        if (err) {
                            winston.debug("[TablesAccountService] Error deleting app entity : ", err);
                            reject(err);
                        } else {
                            resolve(dbInfo);
                        }
                    });
                },
                err => reject(err)
            );
        });
    }

    /**
     * Create entity descriptor
     * @param appSecret 
     * @param dbName 
     * @param dbConnectionString 
     */
    private createEntityDescriptor(appSecret: string, dbName?: string, dbConnectionString?: string): object {
        let entGen = azureStorage.TableUtilities.entityGenerator;
        let descriptor = {
            PartitionKey: entGen.String(PARTITION_KEY),
            RowKey: entGen.String(appSecret),
        };
        if (dbName)
            descriptor["dbName"] = entGen.String(dbName);
        if (dbConnectionString)
            descriptor["dbConnectionString"] = entGen.String(dbConnectionString);

        return descriptor;
    }

}