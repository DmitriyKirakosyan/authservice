import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import CollectionController from '../shared/controller/CollectionController';
import dbConfig from '../DatabaseConfig';
import appConfig from '../ApplicationConfig';
import { respondWithError } from './functionUtils';

module.exports.execute = function(context: any) {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret = context.req.headers[appConfig.appSecretHeader];

    if (!appSecret) {
        respondWithError(context, new Error(appConfig.appSecretHeader + " header must be defined."), 400);
        return;
    }

    let storage = AccountServiceProvider.getDefault();
    storage.getDatabase(appSecret).then((dbInfo) => {

        let collController = new CollectionController(dbInfo.connectionString);
        return collController.getCollections(dbInfo.name);

    }).then(colls => {

        context.res = responseBuilder.setStatus(200).setBody(colls).build();
        context.done();

    }).catch(err => respondWithError(context, err));

};