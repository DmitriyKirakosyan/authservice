import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import CollectionController from '../shared/controller/CollectionController';
import config from '../DatabaseConfig';
import appConfig from '../ApplicationConfig';
import { respondWithError } from './functionUtils';


module.exports.execute = function(context: any) {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret = context.req.headers[appConfig.appSecretHeader];

    if (!context.req.params.collId) {
        respondWithError(context, new Error("Collection id must be defined."), 400);
        return;
    }

    let storage = AccountServiceProvider.getDefault();
    storage.getDatabase(appSecret).then((dbInfo) => {

        let collController = new CollectionController(dbInfo.connectionString);
        return collController.deleteCollection(context.req.params.collId, dbInfo.name);

    }).then(() => {

        context.res = responseBuilder.setStatus(200).build();
        context.done();

    }).catch(err => respondWithError(context, err));

};