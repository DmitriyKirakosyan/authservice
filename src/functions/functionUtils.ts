import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import DatabaseController from "../shared/controller/DatabaseController";
import { ResponseBuilder } from "./ResponseBuilder";
import dbConfig from '../DatabaseConfig';

/**
 * Represents web response error
 */
export class ResponseError {
    constructor(public code: number, public message: string) { }
}

/**
 * Uses {@link context} to respond with Error
 * @param context
 * @param error 
 */
export function respondWithError(context: any, error: Error, code: number): void;
export function respondWithError(context: any, error: any): void;
export function respondWithError(context: any, error: Error | any, code?: number): void {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();

    if (error instanceof Error) {
        code = code || 500;
        context.res = responseBuilder.setStatus(code).setError(error).build();
    } else {
        let code: number = error.code && typeof error.code === 'number' ? error.code : 500;
        context.res = responseBuilder.setStatus(code).setError(error).build();
    }

    context.done();
}