export class ResponseBuilder {
    private status: number;
    private headers: any;
    private body: any;
    private error: any;

    constructor() {
        this.headers = {};
    }

    public setStatus(status: number): ResponseBuilder {
        this.status = status;
        return this;
    }

    public addHeaders(headers: any): ResponseBuilder {
        this.headers = Object.assign(this.headers, headers);
        return this;
    }

    public setBody(body: any): ResponseBuilder {
        this.body = body;
        return this;
    }

    public setError(error: any): ResponseBuilder {
        this.error = error;
        return this;
    }

    public build(): object {
        let toJSON = require('utils-error-to-json');
        let responseBody: any;
        if (!this.error) {
            responseBody = this.body;
        }
        else {
            responseBody = this.error instanceof Error ? toJSON(this.error) : this.error;
        }

        return {
            status: this.status,
            headers: Object.assign(this.getDefaultHeaders(), this.headers),
            body: responseBody
        };
    }

    private getDefaultHeaders(): any {
        return {
            'Content-Type': 'application/json'
        };
    }
}