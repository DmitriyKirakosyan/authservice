import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import CollectionController from '../shared/controller/CollectionController';
import TokenBroker from '../shared/controller/TokenBroker';
import * as jwt from 'jsonwebtoken';
import config from '../DatabaseConfig';
import appConfig from '../ApplicationConfig';
import { respondWithError } from './functionUtils';

module.exports.execute = function (context: any) {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret = context.req.headers[appConfig.appSecretHeader];

    if (!appSecret) {
        respondWithError(context, new Error(appConfig.appSecretHeader + " header must be defined."), 400);
        return;
    }
    if (!context.req.params.collId) {
        respondWithError(context, new Error("Collection id must be defined."), 400);
        return;
    }

    let authToken = context.req.headers[appConfig.authTokenHeader];

    let userId;
    // Using JWT Token to parse user id if user id not passed in header
    if (authToken) {
        let decoded = jwt.decode(authToken) as object;
        // if (decoded && 'stable_sid' in decoded) {
        //     userId = decoded['stable_sid'].substr(4);
        // }
        userId = decoded["sub"];
    }

    // Anonymous access
    if (!userId) {
        userId = "demo_user";
    }

    let storage = AccountServiceProvider.getDefault();
    storage.getDatabase(appSecret).then((dbInfo) => {

        let tokenBroker = new TokenBroker(dbInfo.connectionString);
        return tokenBroker.getUserAccessToken(userId, dbInfo.name, context.req.params.collId);

    }).then(token => {

        context.res = responseBuilder.setStatus(200).setBody(token).build();
        context.done();

    }).catch(err => respondWithError(context, err));

};