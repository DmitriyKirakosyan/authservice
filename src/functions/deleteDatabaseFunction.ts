import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import DatabaseController from "../shared/controller/DatabaseController";
import { respondWithError } from './functionUtils';
import appConfig from '../ApplicationConfig';

module.exports.execute = function (context: any): void {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret: string = context.req.params.appSecret;

    let storage = AccountServiceProvider.getDefault();
    storage.deleteDatabase(appSecret).then((dbInfo) => {
        let databaseController: DatabaseController = new DatabaseController(dbInfo.connectionString);
        return databaseController.deleteDatabase(dbInfo.name);
    }).then(() => {
        context.res = responseBuilder.setStatus(204)
            .build();
        context.done();
    }).catch(err => respondWithError(context, err));

};