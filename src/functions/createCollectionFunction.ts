import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import CollectionController from '../shared/controller/CollectionController';
import config from '../DatabaseConfig';
import appConfig from '../ApplicationConfig';
import { respondWithError } from './functionUtils';

module.exports.execute = function (context: any) {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret = context.req.headers[appConfig.appSecretHeader];

    let collInfo = context.req.body;

    if (!appSecret) {
        respondWithError(context, new Error(appConfig.appSecretHeader + " header must be defined."), 400);
        return;
    }
    if (!collInfo || !collInfo.id) {
        respondWithError(context, new Error("Collection id must be defined."), 400);
        return;
    }

    let storage = AccountServiceProvider.getDefault();
    storage.getDatabase(appSecret).then((dbInfo) => {

        let collController = new CollectionController(dbInfo.connectionString);
        return collInfo.private ?
            collController.createPrivateCollection(collInfo.id, dbInfo.name) :
            collController.createSharedCollection(collInfo.id, dbInfo.name);

    }).then(coll => {

        context.res = responseBuilder.setStatus(200).setBody(coll).build();
        context.done();

    }).catch(err => respondWithError(context, err));

};