import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import appConfig from '../ApplicationConfig';
import dbConfig from '../DatabaseConfig';
import { ResponseBuilder } from "./ResponseBuilder";
import DatabaseController from "../shared/controller/DatabaseController";
import { respondWithError } from './functionUtils';
import { parseConnectionString } from '../shared/model/documentDBUtils';

module.exports.execute = function (context: any): void {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret = context.req.headers[appConfig.appSecretHeader];
    let reqBody = context.req.body;

    if (!appSecret) {
        respondWithError(context, new Error(appConfig.appSecretHeader + " header must be defined."), 400);
        return;
    }

    let storage = AccountServiceProvider.getDefault();
    if (storage.doesAppSeсretExist(appSecret)) {
        respondWithError(context, new Error("This application has already been added."), 409);
        return;
    }


    let connectionString = reqBody.dbConnectionString;
    if (!connectionString) {
        connectionString = dbConfig.defaultConnectionString;
    }

    let response: any;

    storage.addDatabase(appSecret, connectionString).then(
        dbInfo => {
            let account = parseConnectionString(dbInfo.connectionString);
            if (!account.host.endsWith('/')) account.host += '/';
            response = {
                dbUrl: account.host + 'dbs/' + dbInfo.name
            };

            let databaseController: DatabaseController = new DatabaseController(connectionString);
            return databaseController.createDatabase(dbInfo.name);
        }
    ).then(() => {

        context.res = responseBuilder.setStatus(200).setBody(response).build();
        context.done();

    }).catch(err => respondWithError(context, err));

};