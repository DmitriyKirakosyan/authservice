import AccountServiceProvider from '../shared/account/AccountServiceProvider';
import { ResponseBuilder } from "./ResponseBuilder";
import DatabaseController from "../shared/controller/DatabaseController";
import { respondWithError } from './functionUtils';
import appConfig from '../ApplicationConfig';

module.exports.execute = function(context: any): void {
    let responseBuilder: ResponseBuilder = new ResponseBuilder();
    let appSecret: string = context.req.headers[appConfig.appSecretHeader];

    if (!appSecret) {
        respondWithError(context, new Error(appConfig.appSecretHeader + " header must be defined."), 400);
        return;
    }

    let storage = AccountServiceProvider.getDefault();
    storage.getDatabase(appSecret).then((dbInfo) => {
        context.res = responseBuilder.setStatus(200)
            .setBody({
                databaseName: dbInfo.name,
                databaseConnectionString: dbInfo.connectionString
            })
            .build();
        context.done();
    }).catch(err => respondWithError(context, err));

};