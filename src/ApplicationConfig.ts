
export class ApplicationConfig {

    get appSecretHeader(): string {
        return "x-app-secret";
    }

    get userIdHeader(): string {
        return "x-userid";
    }

    get authTokenHeader(): string {
        return "x-zumo-auth";
    }

}

export default new ApplicationConfig();