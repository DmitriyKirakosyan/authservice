import { ConnectionPolicy } from 'documentdb';

export class DatabaseConfig {
    constructor() {
    }

    /// Database Connection Settings

    private get host(): string {
        return process.env.DB_HOST || 'https://localhost:8081';
    }

    private get masterKey(): string {
        return process.env.DB_AUTHKEY || 'C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==';
    }

    get defaultConnectionString(): string {
        return process.env.DOCUMENTDB_CONNECTION_STRING || "AccountEndpoint=" + this.host + ";" + "AccountKey=" + this.masterKey + ";";
    }

    /// Other Settings

    get permissionTokenTTL(): number {
        return 3600;
    }

    get privateCollectionPartitionKey(): string {
        return '_userId';
    }

    /// Debug Settings

    get testDatabaseId(): string {
        return "tempdb";
    }


}

export default new DatabaseConfig();