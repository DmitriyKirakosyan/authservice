"use strict";

let getCollectionsFunction = require('../../out/js/functions/getCollectionsFunction');

module.exports = function (context) {
    getCollectionsFunction.execute(context);
}