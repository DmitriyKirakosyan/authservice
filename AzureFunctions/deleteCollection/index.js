"use strict";

let deleteCollectionFunction = require('../../out/js/functions/deleteCollectionFunction');

module.exports = function (context) {
    deleteCollectionFunction.execute(context);
}