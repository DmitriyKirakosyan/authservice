"use strict";

let createCollectionFunction = require('../../out/js/functions/createCollectionFunction');

module.exports = function (context) {
    createCollectionFunction.execute(context);
}