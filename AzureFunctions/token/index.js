"use strict";

let retrieveTokenFunction = require('../../out/js/functions/retrieveTokenFunction');

module.exports = function (context) {
    retrieveTokenFunction.execute(context);
}