let setupFunction = require('../../out/js/functions/createDatabaseFunction');

/**
 * Headers:
 *  x-app-secert
 * Body:
 * {
 *   (optional) ddbConnectionString: string
 * }
 *
 * Result
 * {
 *   dbUrl: string
 * }
 */

module.exports = function (context) {
    setupFunction.execute(context);
}