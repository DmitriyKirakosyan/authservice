"use strict";

let retrieveDatabaseFunction = require('../../out/js/functions/retrieveDatabaseFunction');

module.exports = function (context) {
    retrieveDatabaseFunction.execute(context);
}