"use strict";

let deleteDatabaseFunction = require('../../out/js/functions/deleteDatabaseFunction');

module.exports = function (context) {
    deleteDatabaseFunction.execute(context);
}