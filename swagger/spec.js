var spec = {
    "swagger": "2.0",
    "info": {
        "version": "1.0.0",
        "title": "mc-thin-backend"
    },
    "tags": [{
        "name": "application",
        "description": "Application setup"
    }, {
        "name": "collection",
        "description": "Database collections management"
    }, {
        "name": "access",
        "description": "User access token broker"
    }],
    "schemes": ["http"],
    "consumes": ["application/json"],
    "produces": ["application/json"],
    "paths": {
        "/app": {
            "get": {
                "tags": ["application"],
                "description": "Retrive Database Info",
                "operationId": "retriveDatabaseInfo",
                "parameters": [{
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "$ref": "#/definitions/RetriveDBInfoResponse"
                        }
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse"
                        }
                    }
                }
            },
            "post": {
                "tags": ["application"],
                "description": "Setup application. Creates database and maps it to the app secret.",
                "operationId": "create",
                "parameters": [{
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "body",
                    "in": "body",
                    "description": "DocumentDB Connection String",
                    "required": false,
                    "schema": {
                        "$ref": "#/definitions/ConnectionString"
                    }
                }],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "$ref": "#/definitions/CreateDBResponse"
                        }
                    }
                }
            },
            "x-swagger-router-controller": "AppRouter"
        },
        "/app/{appSecret}": {
            "delete": {
                "tags": ["application"],
                "description": "Delete existing Application",
                "operationId": "deleteApp",
                "parameters": [{
                    "name": "appSecret",
                    "in": "path",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success"
                    }
                }
            },
            "x-swagger-router-controller": "AppRouter"
        },
        "/colls": {
            "get": {
                "tags": ["collection"],
                "description": "Get all collections",
                "operationId": "listCollections",
                "parameters": [{
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/Collection"
                            }
                        }
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse"
                        }
                    }
                }
            },
            "post": {
                "tags": ["collection"],
                "description": "Create new collection",
                "operationId": "addCollection",
                "parameters": [{
                    "name": "body",
                    "in": "body",
                    "description": "Collection Definition",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/CollectionDefenition"
                    }
                }, {
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "$ref": "#/definitions/Collection"
                        }
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse"
                        }
                    }
                }
            }
        },
        "/colls/{collId}": {
            "delete": {
                "tags": ["collection"],
                "description": "Delete collection",
                "operationId": "deleteCollection",
                "parameters": [{
                    "name": "collId",
                    "in": "path",
                    "description": "Collection name",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success"
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse"
                        }
                    }
                }
            }
        },
        "/token/{collId}": {
            "get": {
                "tags": ["access"],
                "description": "Get all documents",
                "operationId": "listDocuments",
                "parameters": [{
                    "name": "collId",
                    "in": "path",
                    "description": "Collection Name. Example: \"MyCollection\"",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "x-app-secret",
                    "in": "header",
                    "description": "App secret. Provided by Mobile Center.",
                    "required": true,
                    "type": "string"
                }, {
                    "name": "x-auth-token",
                    "in": "header",
                    "description": "User Authorization Token (JWT). e.g. From EasyAuth.",
                    "required": false,
                    "type": "string"
                }],
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "$ref": "#/definitions/AuthToken"
                        }
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse"
                        }
                    }
                }
            }
        },
        "/swagger": {
            "x-swagger-pipe": "swagger_raw"
        }
    },
    "securityDefinitions": {
        "apikey": {
            "type": "apiKey",
            "name": "authorization",
            "in": "header"
        },
        "isquery": {
            "type": "apiKey",
            "name": "x-ms-documentdb-isquery",
            "in": "header"
        }
    },
    "definitions": {
        "AppSecret": {
            "type": "string",
            "description": "Application secret, provided by Mobile Center",
            "example": "0d7bbb02-476a-4d05-a466-b0bb1a951bed"
        },
        "AuthToken": {
            "type": "object",
            "required": ["token", "ttl"],
            "properties": {
                "token": {
                    "type": "string"
                },
                "ttl": {
                    "type": "number"
                }
            },
            "description": "Returns permission token and its living time",
            "example": {
                "token": "<CosmosDB Permission Token>",
                "ttl": 3600
            }
        },
        "CreateDBResponse": {
            "type": "object",
            "required": ["dbUrl"],
            "properties": {
                "dbUrl": {
                    "type": "string"
                }
            },
            "description": "Create Database Result",
            "example": {
                "dbUrl": "sampledb.documents.azure.com:443/dbs/dbName"
            }
        },
        "RetriveDBInfoResponse": {
            "type": "object",
            "required": ["databaseName", "databaseConnectionString"],
            "properties": {
                "databaseName": {
                    "type": "string"
                },
                "databaseConnectionString": {
                    "type": "string"
                }
            },
            "description": "Database Information",
            "example": {
                "databaseName": "db_cc1afedf-723a-4073-80b1-c93971520de1",
                "databaseConnectionString": "AccountEndpoint=https://sampledb.documents.azure.com:443/;AccountKey=2hMHoUJZdsd9eJyz1HMLgB0cw43Xqerwpyi88c2mLfjaabJxudXg1Xdf4dzejofRmstbIcdkimz7N1AF7liOBw==;"
            }
        },
        "ConnectionString": {
            "type": "object",
            "properties": {
                "dbConnectionString": {
                    "type": "string"
                }
            },
            "example": {
                "dbConnectionString": "AccountEndpoint=https://sampledb.documents.azure.com:443/;AccountKey=2hMHoUJZ5xd9eJyz1HMLgB0cw32Xqerwpyi88c2mLfjaabJxudXg1Xif4dzejofRmstbIcdkimz7N1AF7liOBw==;"
            }
        },
        "CollectionDefenition": {
            "type": "object",
            "required": ["id"],
            "properties": {
                "id": {
                    "type": "string"
                },
                "private": {
                    "type": "boolean"
                }
            },
            "example": {
                "id": "collectionId",
                "private": true
            }
        },
        "Collection": {
            "type": "object",
            "required": ["id"],
            "properties": {
                "id": {
                    "type": "string"
                }
            },
            "example": {
                "id": "collectionId"
            }
        },
        "ErrorResponse": {
            "type": "object",
            "required": ["code", "message"],
            "properties": {
                "message": {
                    "type": "object",
                    "example": {
                        "Errors": ["Resource Not Found"]
                    },
                    "properties": {}
                },
                "code": {
                    "type": "string"
                }
            }
        }
    }
}