import * as deleteCollFunc from '../src/functions/deleteCollectionFunction';
import * as assert from 'assert';
import appConfig from '../src/ApplicationConfig';

import { createAccountAndCollection, deleteAccount } from './testUtils';


describe("Test Function: Create Collection", () => {
    let dbName = "test_db";
    let appSecret = "test-app-secert";
    let collName = "test_coll";

    before(function (done) {
        this.timeout(10000);

        createAccountAndCollection(appSecret, { id: collName, private: true }, done);
    });

    after(done => deleteAccount(appSecret, done));


    it('It should delete collection for app secret', function (done) {
        this.timeout(4000);
        let context = {
            req: {
                headers: {
                    [appConfig.appSecretHeader]: appSecret
                },
                params: { collId: collName }
            },
            done: () => {
                if (context["res"]["status"] === 200)
                    done();
                else
                    done(context["res"]["body"]);
            }
        };
        deleteCollFunc["execute"](context);

    });
});