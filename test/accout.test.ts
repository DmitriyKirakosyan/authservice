import { AbstractAccountService } from '../src/shared/account/AbstractAccountService';
import JSONAccountService from '../src/shared/account/JSONAccountService';
import TablesAccountService from '../src/shared/account/TablesAccountService';
import DatabaseController from '../src/shared/controller/DatabaseController';
import * as assert from 'assert';

import { config } from "dotenv";
config();

let testServiceAccount = (serviceName: string, serviceInstance: AbstractAccountService) => {
    describe(serviceName + " tests", () => {
        let appSecret = "test-app-secert";

        let deleteDB = (done) => {
            let storage = serviceInstance;

            storage.deleteDatabase(appSecret).then(
                dbInfo => {
                    // remove database if exist
                    if (!dbInfo.connectionString) {
                        done();
                        return;
                    }
                    let databaseController: DatabaseController = new DatabaseController(dbInfo.connectionString);
                    databaseController.deleteDatabase(dbInfo.name).then(
                        () => done(),
                        (err) => done(err)
                    );
                },
                err => done(err)
            );
        };

        before(function (done) {
            this.timeout(4000);
            let storage = serviceInstance;

            // we don't care if there is no database and we get error because of that
            // we just want to ensure that before test executing database does not exist
            deleteDB(() => done());
        });

        it('It should create database based on application secret', done => {
            let storage = serviceInstance;

            let connectionString = process.env.DOCUMENTDB_CONNECTION_STRING;
            let dbName: string;
            storage.addDatabase(appSecret, connectionString).then(
                dbInfo => {
                    assert.equal(dbInfo.connectionString, connectionString);
                    assert.equal(dbInfo.name.length, 39);
                    assert.equal(dbInfo.name.substr(0, 3), "db_");

                    dbName = dbInfo.name;

                    return storage.deleteDatabase(appSecret);
                }
            ).then(dbInfo => {
                assert.equal(dbName, dbInfo.name);
                assert.equal(dbInfo.connectionString, connectionString);
                done();
            }).catch(err => done(err));
        });

    });
};


testServiceAccount('JSON Account Service', new JSONAccountService());
testServiceAccount('Azure Tables Account Service', new TablesAccountService());