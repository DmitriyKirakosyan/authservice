import DbController from '../src/shared/controller/DatabaseController';
import * as createDBFunc from '../src/functions/createDatabaseFunction';
import * as assert from 'assert';
import appConfig from '../src/ApplicationConfig';
import dbConfig from '../src/DatabaseConfig';

import { deleteAccount } from './testUtils';


describe("Test Function: Create Database", () => {
    let dbName = "test_db";
    let appSecret = "test-app-secert";
    let connectionString = dbConfig.defaultConnectionString;

    // remove account before and after the test
    before(done => deleteAccount(appSecret, done));
    after(done => deleteAccount(appSecret, done));

    it('It should create database for app secret', function (done) {
        this.timeout(4000);
        let context = {
            req: {
                headers: {
                    [appConfig.appSecretHeader]: appSecret
                },
                body: {
                    dbConnectionString: connectionString
                }
            },
            done: () => {
                // assert.equal(context["res"]["status"], 200);

                // let result: { dbUrl: string } = context["res"]["body"];
                // assert.equal(result.dbUrl.indexOf("test-account-endpoint/"), 0);
                // assert.equal(result.dbUrl.substring("test-account-endpoint/".length - 1).length, 39);
                if (context["res"]["status"] === 200)
                    done();
                else
                    done(context["res"]["body"]);
            }
        };
        createDBFunc["execute"](context);
    });
});