import * as retriveDBFunc from '../src/functions/retrieveDatabaseFunction';
import * as assert from 'assert';
import appConfig from '../src/ApplicationConfig';

import { createAccount, deleteAccount } from './testUtils';

describe("Test Function: Retrive Database", () => {
    let dbName = "test_db";
    let appSecret = "test-app-secert";

    before(function (done) {
        this.timeout(10000);

        createAccount(appSecret, done);
    });

    after(done => deleteAccount(appSecret, done));

    it('It should retrive database for app secret', function (done) {
        this.timeout(4000);
        let context = {
            req: {
                headers: {
                    [appConfig.appSecretHeader]: appSecret
                }
            },
            done: () => {
                if (context["res"]["status"] === 200)
                    done();
                else
                    done(context["res"]["body"]);
            }
        };
        retriveDBFunc["execute"](context);

    });
});