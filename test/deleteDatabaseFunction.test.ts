import * as deleteDBFunc from '../src/functions/deleteDatabaseFunction';
import * as assert from 'assert';
import appConfig from '../src/ApplicationConfig';

import { createAccount } from './testUtils';

describe("Test Function: Delete Database", () => {
    let dbName = "test_db";
    let appSecret = "test-app-secert";

    before(function (done) {
        this.timeout(10000);

        createAccount(appSecret, done);
    });

    it('It should delete database for app secret', function (done) {
        this.timeout(4000);
        let context = {
            req: {
                headers: {},
                params: { appSecret: appSecret }
            },
            done: () => {
                if (context["res"]["status"] === 200)
                    done();
                else
                    done(context["res"]["body"]);
            }
        };
        deleteDBFunc["execute"](context);

    });
});