import * as createCollFunc from '../src/functions/createCollectionFunction';
import * as assert from 'assert';
import appConfig from '../src/ApplicationConfig';

import { createAccount, deleteAccount } from './testUtils';


describe("Test Function: Create Collection", () => {
    let dbName = "test_db";
    let appSecret = "test-app-secert";
    let collName = "test_coll";

    before(function (done) {
        this.timeout(10000);

        createAccount(appSecret, done);
    });

    after(done => deleteAccount(appSecret, done));

    it('It should create collection for app secret', function (done) {
        this.timeout(4000);
        let context = {
            req: {
                headers: {
                    [appConfig.appSecretHeader]: appSecret
                },
                body: {
                    id: collName,
                    private: true
                }
            },
            done: () => {
                if (context["res"]["status"] === 200)
                    done();
                else
                    done(context["res"]["body"]);
            }
        };
        createCollFunc["execute"](context);

    });
});