import TokenBroker from "../src/shared/controller/TokenBroker";
import DbController from '../src/shared/controller/DatabaseController';
import CollController from '../src/shared/controller/CollectionController';
import * as assert from 'assert';
import dbConfig from '../src/DatabaseConfig';

describe("Token Broker Tests", () => {
    let dbName = "test_db";
    let collName = "test_coll";
    let userName = "test_user";

    before(function (done) {
        this.timeout(4000);
        let dbController = new DbController(dbConfig.defaultConnectionString);
        let collController = new CollController(dbConfig.defaultConnectionString);

        let createEnvironment = () => {
            dbController.createDatabase(dbName)
                .then(dbMeta => {
                    return collController.createPrivateCollection(collName, dbName);
                })
                .then(collMeta => done())
                .catch(err => done(err));
        };

        dbController.deleteDatabase(dbName).then(
            done => createEnvironment(),
            err => createEnvironment()
        );
    });

    it('It should return valid access token', done => {
        let broker = new TokenBroker(dbConfig.defaultConnectionString);

        broker.getUserAccessToken(userName, dbName, collName).then(
            tokenObj => {
                assert.equal(typeof tokenObj["token"], "string");
                assert.equal(typeof tokenObj["ttl"], "number");
                done();
            },
            error => {
                done(error);
            }
        );
    });
})