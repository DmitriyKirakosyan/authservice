import AccountServiceProvider from '../src/shared/account/AccountServiceProvider';
import DatabaseController from '../src/shared/controller/DatabaseController';
import CollectionController from '../src/shared/controller/CollectionController';
import dbConfig from '../src/DatabaseConfig';
import { parseConnectionString } from '../src/shared/model/documentDBUtils';


export function deleteAccount(appSecret: string, done) {

    let storage = AccountServiceProvider.getDefault();
    storage.deleteDatabase(appSecret).then((dbInfo) => {
        let databaseController: DatabaseController = new DatabaseController(dbInfo.connectionString);
        return databaseController.deleteDatabase(dbInfo.name);
    })
        .then(() => done())
        .catch(err => done());
}

export function createAccount(appSecret: string, done) {
    deleteAccount(appSecret, () => {
        let storage = AccountServiceProvider.getDefault();
        let connectionString = dbConfig.defaultConnectionString;
        storage.addDatabase(appSecret, connectionString).then(
            dbInfo => {
                let databaseController: DatabaseController = new DatabaseController(connectionString);
                return databaseController.createDatabase(dbInfo.name);
            }
        )
            .then(() => done())
            .catch(err => done());
    });
}

export function createAccountAndCollection(appSecret: string, collInfo: any, done) {
    createAccount(appSecret, () => {
        let storage = AccountServiceProvider.getDefault();
        storage.getDatabase(appSecret).then((dbInfo) => {

            let collController = new CollectionController(dbInfo.connectionString);
            return collInfo.private ?
                collController.createPrivateCollection(collInfo.id, dbInfo.name) :
                collController.createSharedCollection(collInfo.id, dbInfo.name);
        })
            .then(() => done())
            .catch(err => done());

    });
}

