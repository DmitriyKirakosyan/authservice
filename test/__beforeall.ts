
import { config } from "dotenv";
import * as winston from "winston";

config();

before(function () {
    winston.configure({
        level: "debug",
        transports: [
            new (winston.transports.Console)()
        ]
    });

});