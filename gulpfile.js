var gulp = require('gulp');
var ts = require('gulp-typescript');
var tslint = require('gulp-tslint');
var exec = require('child_process').exec;

const tsProject = ts.createProject('tsconfig.json');

gulp.task('build', function () {
    var tsResult = tsProject.src().pipe(tsProject());
    tsResult.dts.pipe(gulp.dest('out/dts'));
    return tsResult.js.pipe(gulp.dest('out/js'));
});

gulp.task('lint', function () {

})

gulp.task('docs', () => {
    let openCommand = /^win/.test(process.platform) ? 'start' : 'open';
    // exec(openCommand + ' swagger/index.html');
    exec('open swagger/index.html');
})