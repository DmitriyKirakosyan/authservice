
# MC Storage Token Broker

## Local Run

```shell
$ npm i
$ npm run config
$ npm start
```

Script `npm run config` will ask you databases information.
Note, that last question should be answered `yes`, if you are using local documentdb emulator.

## Deploy to Azure Functions

1. Log in to your subscription and create a new Function App;
2. Navigate to Platform features -> Application Settings;
3. Add Key DOCUMENTDB_CONNECTION_STRING, and set your documentdb connection string as a Value;
4. Add Key AZURE_STORAGE_CONNECTION_STRING, and set your azure storage connection string as a Value;
5. Add Key STORAGE_TYPE, and set 'tables' as a Value;
6. Navigate to Platform features -> Deployment options, and setup code syncronization via OneDrive;

7. Click Setup button and choose OneDrive option. Authorizate and choose folder;
8. Once the app is syncronized with OneDrive, you have empty folder for azure function app on your OneDrive disc under 'Files/Apps/Azure Web Apps';
9. Run script `npm run deploy_af`. Copy all files from `./out` to your app folder on OneDrive;
10. Go back to your azure function app deployment options and click Synnc button;
11. Once sync is over, your function app is ready to use.


## Deploy to Azure App Service/Web Application

1. Log in to your subscription and craete a new empty Node js Web Application;
2. Repeat 2-8 steps from `Deploy to Azure Functions` section;
3. Add key WEBSITE_NODE_DEFAULT_VERSION, and set 8.0.0 as a Value.
4. Copy files `server.js`, `package.json`, `tsconfig.json` and `gulpfile.js` to OneDrive application folder;
5. Copy folders `src` and `AzureFunctions` to OneDrive application folder;
6. Go back to your web app deployment options and click Sync button;
7. Navigate to Application Editor blade and open editor;
8. Open console and run commands:
```sh
$ npm i
$ npm run build
```
9. Click Run button.


## Tests

```shell
$ npm test
```

## Swagger Docs

```shell
$ npm run swagger
```