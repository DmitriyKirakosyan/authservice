#!/usr/bin/env node

"use strict";

var afRunner = require("./node_modules/azure-functions-runner/js/AFRunner");
const runner = new afRunner.AFRunner('AzureFunctions');
runner.setPort = process.env.PORT || 3000;
runner.start();